﻿using Bookshelf.Core.Interfaces;
using Bookshelf.Infrastructure.Data;
using Bookshelf.Infrastructure.Services.BookshelfService;
using Microsoft.Extensions.DependencyInjection;

namespace Bookshelf.Web.Configurations
{
    public class ServiceConfig
    {
        public void Configuration(IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<IBookshelfService, BookshelfService>();
        }
    }
}
