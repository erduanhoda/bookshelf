﻿using Bookshelf.Core.Entities;
using Bookshelf.Infrastructure.Services.BookshelfService;
using Microsoft.AspNetCore.Mvc;

namespace Bookshelf.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Bookshelfs")]
    public class BookshelfsController : Controller
    {
        private readonly IBookshelfService _bookshelfService;

        public BookshelfsController(IBookshelfService bookshelfService)
        {
            _bookshelfService = bookshelfService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var books = _bookshelfService.Books;
            return Ok(books);
        }

        [Route("loaned/book/{bookId}/lender/{lenderId}"), HttpPut]
        public IActionResult Loaned(Book book)
        {
            var loaned = _bookshelfService.Loaned(book);

            if (loaned)
                return Ok(loaned);
            return BadRequest("Something wrong happened! Please try again.");
        }

        [Route("returned/book/{bookId}"), HttpPut]
        public IActionResult Returned(Book book)
        {
            var returned = _bookshelfService.Returned(book);

            if (returned)
                return Ok(returned);
            return BadRequest("Something wrong happened! Please try again.");
        }
    }
}