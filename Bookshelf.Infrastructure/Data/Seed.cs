﻿using Bookshelf.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bookshelf.Infrastructure.Data
{
    public class Seed
    {
        public static async System.Threading.Tasks.Task SeedAsync(BookshelfContext bookshelfContext, int? retry = 0)
        {

            int retryForAvailability = retry.Value;
            try
            {
                // TODO: Only run this if using a real database
                bookshelfContext.Database.Migrate();

                if (!bookshelfContext.Books.Any())
                {
                    bookshelfContext.Books.AddRange(GetPreconfiguredBooks());

                    await bookshelfContext.SaveChangesAsync();
                }

                if (!bookshelfContext.Lenders.Any())
                {
                    bookshelfContext.Lenders.AddRange(GetPreconfiguredLenders());

                    await bookshelfContext.SaveChangesAsync();
                }

            }
            catch (Exception ex)
            {

            }
        }

        static List<Book> GetPreconfiguredBooks()
        {
            return new List<Book>(){
                new Book()
                {
                    Title = "The King",
                    Author = "Rich Koslowski",
                    ISBN = "978-1-891830-65-5",
                    CreatedAt = DateTime.Now,
                    UpdateddAt = DateTime.Now
                },
                new Book()
                {
                    Title = "Less Than Heroes",
                    Author = "David Yurkovich",
                    ISBN = "978-1-891830-51-8",
                    CreatedAt = DateTime.Now,
                    UpdateddAt = DateTime.Now
                }, new Book()
                {
                    Title = "Mosquito",
                    Author = "Dan James",
                    ISBN = "978-1-891830-68-6",
                    CreatedAt = DateTime.Now,
                    UpdateddAt = DateTime.Now
                }, new Book()
                {
                    Title = "Spiral-Bound",
                    Author = "Aaron Renier",
                    ISBN = "978-1-891830-50-1",
                    CreatedAt = DateTime.Now,
                    UpdateddAt = DateTime.Now
                }, new Book()
                {
                    Title = "Three Fingers",
                    Author = "Rich Koslowski",
                    ISBN = "978-1-891830-31-0",
                    CreatedAt = DateTime.Now,
                    UpdateddAt = DateTime.Now
                }, new Book()
                {
                    Title = "Bighead",
                    Author = "Jeffrey Brown",
                    ISBN = "978-1-891830-56-3",
                    CreatedAt = DateTime.Now,
                    UpdateddAt = DateTime.Now
                }, new Book()
                {
                    Title = "Beach Safari",
                    Author = "Mawil",
                    ISBN = "978-1-891830-40-2",
                    CreatedAt = DateTime.Now,
                    UpdateddAt = DateTime.Now
                }, new Book()
                {
                    Title = "American Elf",
                    Author = "James Kochalka",
                    ISBN = "978-1-60309-2395",
                    CreatedAt = DateTime.Now,
                    UpdateddAt = DateTime.Now
                }
            };
        }

        static List<User> GetPreconfiguredLenders()
        {
            return new List<User>(){
                new User()
                {
                    FirstName = "John",
                    LastName = "Doe",
                    Username = "john.doe",
                    CreatedAt = DateTime.Now,
                    UpdateddAt = DateTime.Now
                },
                new User()
                {
                    FirstName = "John",
                    LastName = "Smith",
                    Username = "john.smith",
                    CreatedAt = DateTime.Now,
                    UpdateddAt = DateTime.Now
                }, new User()
                {
                    FirstName = "Jane",
                    LastName = "Doe",
                    Username = "jane.doe",
                    CreatedAt = DateTime.Now,
                    UpdateddAt = DateTime.Now
                }, new User()
                {
                    FirstName = "Jane",
                    LastName = "Smith",
                    Username = "jane.smith",
                    CreatedAt = DateTime.Now,
                    UpdateddAt = DateTime.Now
                }, new User()
                {
                    FirstName = "Joe",
                    LastName = "Bloggs",
                    Username = "joe.bloggs",
                    CreatedAt = DateTime.Now,
                    UpdateddAt = DateTime.Now
                }
            };
        }
    }
}