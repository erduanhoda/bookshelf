﻿using System.Collections.Generic;
using Bookshelf.Core.Entities;
using Bookshelf.Core.Interfaces;

namespace Bookshelf.Infrastructure.Services.BookshelfService
{
    public class BookshelfService : IBookshelfService
    {
        private readonly IRepository<Book> _bookRepository;

        public BookshelfService(IRepository<Book> bookRepository)
        {
            _bookRepository = bookRepository;
        }

        public IEnumerable<Book> Books => _bookRepository.ListAll();

        public bool Loaned(Book book)
        {
            if (book.LoanedTo.HasValue)
            {
                var updated = _bookRepository.Update(book);
                if (updated == 1)
                    return true;
            }
            return false;
        }

        public bool Returned(Book book)
        {
            book.LoanedTo = null;
            if (book.LoanedTo.HasValue)
            {
                var updated = _bookRepository.Update(book);
                if (updated == 1)
                    return true;
            }
            return false;
        }
    }
}
