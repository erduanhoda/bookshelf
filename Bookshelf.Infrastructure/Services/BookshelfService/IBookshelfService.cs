﻿using Bookshelf.Core.Entities;
using System.Collections.Generic;

namespace Bookshelf.Infrastructure.Services.BookshelfService
{
    public interface IBookshelfService
    {
        IEnumerable<Book> Books { get; }
        bool Loaned(Book book);
        bool Returned(Book book);
    }
}
