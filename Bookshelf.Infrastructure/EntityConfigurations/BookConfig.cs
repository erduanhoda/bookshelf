﻿using Bookshelf.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bookshelf.Infrastructure.EntityConfigurations
{
    public class BookConfig : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Title).HasMaxLength(100).IsRequired();
            builder.Property(x => x.Author).HasMaxLength(100).IsRequired();
            builder.Property(x => x.ISBN).HasMaxLength(100).IsRequired();
            builder.Property(x => x.LoanedTo).HasMaxLength(100).IsRequired();
            builder.HasQueryFilter(x => !x.IsDeleted);

            builder.HasOne(x => x.Lender).WithMany(x => x.Books).HasForeignKey(x => x.LoanedTo).IsRequired(false);
        }
    }
}
