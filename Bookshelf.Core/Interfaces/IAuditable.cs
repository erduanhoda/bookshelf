﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookshelf.Core.Interfaces
{
    public interface IAuditable
    {
        DateTime CreatedAt { get; set; }
        Guid CreatedBy { get; set; }
        DateTime UpdateddAt { get; set; }
        Guid UpdatedBy { get; set; }
        bool IsDeleted { get; set; }
    }
}
