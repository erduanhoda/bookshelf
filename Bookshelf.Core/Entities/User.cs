﻿using Bookshelf.Core.Interfaces;
using System;
using System.Collections.Generic;

namespace Bookshelf.Core.Entities
{
    public class User : BaseEntity, IAuditable
    {
        public User()
        {
            Books = new HashSet<Book>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }

        public DateTime CreatedAt { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime UpdateddAt { get; set; }
        public Guid UpdatedBy { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}
