﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookshelf.Core.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
