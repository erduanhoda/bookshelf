﻿using System;
using Bookshelf.Core.Interfaces;

namespace Bookshelf.Core.Entities
{
    public class Book : BaseEntity, IAuditable
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string ISBN { get; set; }
        public int? LoanedTo { get; set; }

        public DateTime CreatedAt { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime UpdateddAt { get; set; }
        public Guid UpdatedBy { get; set; }
        public bool IsDeleted { get; set; }

        public User Lender { get; set; }
    }
}
